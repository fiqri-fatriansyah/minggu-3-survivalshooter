﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothing = 5f;
    private Vector3 _offset;

    // Start is called before the first frame update
    void Start()
    {
        //Dapatkan offset antara target dan camera.
        _offset = transform.position - target.position;
    }
    
    //Frame-rate independent FixedUpdate message for physics calculations.
    void FixedUpdate()
    {
        //Dapatkan posisi untuk camera.
        Vector3 targetCampPos = target.position + _offset;

        //Set posisi camera dengan smoothing.
        transform.position = Vector3.Lerp(transform.position, targetCampPos, smoothing * Time.deltaTime);
    }
}

