﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);

    Animator _anim;
    AudioSource _playerAudio;
    PlayerMovement _playerMovement;
    PlayerShooting _playerShooting;
    bool _isDead;                                                
    bool _damaged;                                               

    void Awake()
    {
        //Dapatkan reference component.
        _anim = GetComponent<Animator>();
        _playerAudio = GetComponent<AudioSource>();
        _playerMovement = GetComponent<PlayerMovement>();

        _playerShooting = GetComponentInChildren<PlayerShooting>();
        currentHealth = startingHealth;
    }

    void Update()
    {
        //Jika terkena damage, ubah warna gambar menjadi value dari flashColour.
        if (_damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            //Jika tidak, fade out gambar damage.
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        //Set damaged menjadi false.
        _damaged = false;
    }

    //Method bila player terkena damage.
    public void TakeDamage(int amount)
    {
        _damaged = true;

        //Kurangi health.
        currentHealth -= amount;

        //Sesuaikan slider dengan health player saat ini.
        healthSlider.value = currentHealth;

        //Mainkan suara "damaged".
        _playerAudio.Play();

        //Panggil method Death bila darah <= 0 dan belum mati.
        if (currentHealth <= 0 && !_isDead)
        {
            Death();
        }
    }

    void Death()
    {
        _isDead = true;

        _playerShooting.DisableEffects();

        //Transisi animasi ke "Die".
        _anim.SetTrigger("Die");

        //Mainkan suara "death".
        _playerAudio.clip = deathClip;
        _playerAudio.Play();

        //Matikan script playerMovement.
        _playerMovement.enabled = false;

        _playerShooting.enabled = false;
    }

    public void RestartLevel()
    {
        //Load ulang scene dengan index 0 pada build setting.
        SceneManager.LoadScene(0);
    }
}
