﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;
    private Vector3 _movement;
    private Animator _anim;
    private Rigidbody _playerRigidbody;
    private int _floorMask;
    private float _camRayLength = 100f;

    //Awake is called when the script instance is being loaded.
    void Awake()
    {
        //Dapatkan nilai mask dari layer bernama Floor.
        _floorMask = LayerMask.GetMask("Floor");

        //Dapatkan komponen Animator.
        _anim = GetComponent<Animator>();

        //Dapatkan komponen Rigidbody.
        _playerRigidbody = GetComponent<Rigidbody>();
    }

    //Frame-rate independent FixedUpdate message for physics calculations.
    void FixedUpdate()
    {
        //Dapatkan nilai input horizontal (-1, 0, 1).
        float h = Input.GetAxisRaw("Horizontal");

        //Dapatkan nilai input vertikal (-1, 0, 1).
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h, v);
    }

    //Method pergerakan player.
    public void Move(float h, float v)
    {
        //Set nilai x dan y.
        _movement.Set(h, 0, v);

        //Normalisasi nilai vector agar total panjang dari vektor bernilai 1.
        _movement = _movement.normalized * speed * Time.deltaTime;

        //Gerakkan player ke posisi.
        _playerRigidbody.MovePosition(transform.position + _movement);
    }

    //Method rotasi player.
    void Turning()
    {
        //Buat Ray berdasarkan dari posisi mouse di layar.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Buat raycast untuk floorHit.
        RaycastHit floorHit;

        //Lakukan raycast.
        if(Physics.Raycast(camRay, out floorHit, _camRayLength, _floorMask))
        {
            //Dapatkan vector dari posisi player dan posisi floorHit.
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            //Dapatkan look rotation baru ke hit position.
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            //Rotasi player.
            _playerRigidbody.MoveRotation(newRotation);
        }
    }

    //Method animasi player.
    public void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        _anim.SetBool("IsWalking", walking);
    }
}
