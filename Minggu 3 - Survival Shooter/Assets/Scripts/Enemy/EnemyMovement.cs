﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    private Transform _player;
    private PlayerHealth _playerHealth;
    private EnemyHealth _enemyHealth;
    private UnityEngine.AI.NavMeshAgent _nav;

    void Awake()
    {
        //Cari game object dengan tag "player".
        _player = GameObject.FindGameObjectWithTag("Player").transform;

        //Ambil Reference component
        _playerHealth = _player.GetComponent<PlayerHealth>();
        _enemyHealth = GetComponent<EnemyHealth>();
        _nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    void Update()
    {
        //Jika enemy dan player masih mempunyai health di atas 0, arahkan pergerakan enemy ke player.
        if(_enemyHealth.currentHealth > 0 && _playerHealth.currentHealth > 0)
        {
            _nav.SetDestination(_player.position);
        }
        else //Berhenti.
        {
            _nav.enabled = false;
        }
    }
}
