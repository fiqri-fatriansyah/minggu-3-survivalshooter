﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;

    Animator _anim;
    AudioSource _enemyAudio;
    ParticleSystem _hitParticles;
    CapsuleCollider _capsuleCollider;
    bool _isDead;
    bool _isSinking;

    void Awake()
    {
        //Dapatkan reference component.
        _anim = GetComponent<Animator>();
        _enemyAudio = GetComponent<AudioSource>();
        _hitParticles = GetComponentInChildren<ParticleSystem>();
        _capsuleCollider = GetComponent<CapsuleCollider>();

        //Set health saat ini dengan startingHealth.
        currentHealth = startingHealth;
    }

    void Update()
    {
        //Cek bila enemy tenggelam.
        if(_isSinking)
        {
            //Pindahkan objek ke bawah.
            transform.Translate(-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }

    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        //Cek bila enemy mati.
        if(_isDead)
            return;

        //Mainkan audio.
        _enemyAudio.Play();

        //Kurangi health.
        currentHealth -= amount;

        //Ganti posisi partikel.
        _hitParticles.transform.position = hitPoint;

        //Jalankan sistem partikel.
        _hitParticles.Play();

        //Jika health <= 0, enemy mati.
        if(currentHealth <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        //Set isDead.
        _isDead = true;

        //Set capsuleCollider menjadi trigger.
        _capsuleCollider.isTrigger = true;

        //Jalankan animasi mati.
        _anim.SetTrigger("Dead");

        //Jalankan audio mati.
        _enemyAudio.clip = deathClip;
        _enemyAudio.Play();
    }

    public void StartSinking()
    {
        //Matikan nav mesh component.
        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;

        //Set Rigidbody menjadi kinematic.
        GetComponent<Rigidbody>().isKinematic = true;

        _isSinking = true;
        ScoreManager.score += scoreValue;
        Destroy(gameObject, 2f);
    }
}
