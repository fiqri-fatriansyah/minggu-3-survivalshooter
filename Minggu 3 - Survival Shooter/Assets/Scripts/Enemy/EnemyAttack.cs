﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;

    Animator _anim;
    GameObject _player;
    PlayerHealth _playerHealth;
    EnemyHealth _enemyHealth;
    bool _playerInRange;
    float _timer;

    void Awake ()
    {
        //Cari game object dengan tag "Player".
        _player = GameObject.FindGameObjectWithTag ("Player");

        //Dapatkan component "PlayerHealth".
        _playerHealth = _player.GetComponent <PlayerHealth> ();

        //Dapatkan component "Animator".
        _anim = GetComponent <Animator> ();

        //Dapatkan health enemy.
        _enemyHealth = GetComponent<EnemyHealth>();
    }
    
    //Callback jika ada suatu objek yang memasuki trigger.
    void OnTriggerEnter (Collider other)
    {
        //Set player masuk dalam range.
        if(other.gameObject == _player && other.isTrigger == false)
        {
            _playerInRange = true;
        }
    }

    //Callback jika ada suatu objek yang keluar dari trigger.
    void OnTriggerExit (Collider other)
    {
        //Set player keluar range.
        if(other.gameObject == _player)
        {
            _playerInRange = false;
        }
    }

    void Update()
    {
        _timer += Time.deltaTime;

        if(_timer >= timeBetweenAttacks && _playerInRange && _enemyHealth.currentHealth > 0)
        {
            Attack();
        }

        //Jalankan trigger animasi "playerDead" jika darah player <= 0.
        if(_playerHealth.currentHealth <= 0)
        {
            _anim.SetTrigger("PlayerDead");
        }
    }

    void Attack ()
    {
        //Reset timer.
        _timer = 0f;

        //Jika darah pemain > 0, pemain terkena damage dan darahnya akan berkurang.
        if (_playerHealth.currentHealth > 0)
        {
            _playerHealth.TakeDamage(attackDamage);
        }
    }
}
