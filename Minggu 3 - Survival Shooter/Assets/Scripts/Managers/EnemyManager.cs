﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;

    [SerializeField]
    private MonoBehaviour factory;
    private IFactory Factory { get { return factory as IFactory; } }

    void Start ()
    {
        //Eksekusi fungsi Spawn setiap beberapa detik sesuai dengan nilai "spawnTime".
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    void Spawn ()
    {
        //Jika player telah mati, enemy akan stop dibuat.
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }
        
        //Dapatkan nilai random.
        int spawnPointIndex = Random.Range (0, spawnPoints.Length);
        int spawnEnemy = Random.Range(0, 3);

        //Duplikat enemy (buat enemy) baru.
        Factory.FactoryMethod(spawnEnemy);
    }
}
